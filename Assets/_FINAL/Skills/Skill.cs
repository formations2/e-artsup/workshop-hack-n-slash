﻿using System.Collections.Generic;

using UnityEngine;

///<summary>
/// A Skill is an action a SkillUser can trigger, that can have different effects.
/// An inheritor of Skill class will define the operations to execte when it's triggered, and how/when effects can be applied.
///</summary>
public abstract class Skill : ScriptableObject
{

    [SerializeField]
    private List<SkillMod> m_Mods = new List<SkillMod>();

    /// <summary>
    /// Initialize this skill. Called when the SkillUser that uses this skill is loaded.
    /// </summary>
    /// <param name="_User">The SkillUser that uses this skill.</param>
    public virtual void Init(SkillsUser _User) { }

    /// <summary>
    /// Checks if this skill can be used.
    /// Useful to check for cooldowns for example.
    /// </summary>
    /// <param name="_User">The SkillUser that uses this skill.</param>
    /// <param name="_JsonData">The input data of this skill. Note that your overrides should use unboxing, but test the value type before
    /// manipulating it.</param>
    /// <returns>Returns true if the skill can be used, otherwise false.</returns>
    public abstract bool CanUse(SkillsUser _User, string _JsonData);

    /// <summary>
    /// Called when this skill is used by a SkillUser.
    /// Assumes that CanUse() has been called to check if this skill can effectively be used or not.
    /// </summary>
    /// <param name="_User">The SkillUser that uses this skill.</param>
    /// <param name="_JsonData">The input data of this skill. Note that your overrides should use unboxing, but test the value type before
    /// manipulating it.</param>
    public abstract void Use(SkillsUser _User, string _JsonData);

    /// <summary>
    /// Updates this skill. This is called each frame while a SkillUser has this skill equipped.
    /// Useful for making cooldowns or effects along time.
    /// </summary>
    /// <param name="_User">The Skilluser that has this skill equipped.</param>
    /// <param name="_JsonData">Eventual data to use on update.</param>
    /// <param name="_DeltaTime">Elapsed time from the last frame.</param>
    public virtual void UpdateSkill(SkillsUser _User, string _JsonData, float _DeltaTime) { }

    /// <summary>
    /// Draws Gizmos when this skill is equipped.
    /// IMPORTANT: When SkillUser is initialized, it will duplicate every skill asset. But when this method is called in edit mode, it's
    /// still the original asset. You must not change any value of this Skill instance in this method!
    /// Note that this method is called each frame, even in edit mode.
    /// </summary>
    /// <param name="_User">The Skilluser that has this skill equipped.</param>
    /// <param name="_JsonData">Eventual data to use.</param>
    public virtual void DrawGizmos(SkillsUser _User, string _JsonData) { }

    protected SkillMod[] Mods
    {
        get { return m_Mods.ToArray(); }
    }

}