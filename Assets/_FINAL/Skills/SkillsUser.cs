﻿using System.Collections.Generic;

using UnityEngine;

///<summary>
/// Entry point of the skills system.
/// An element with this component is able to have skills and use them.
///</summary>
[AddComponentMenu("Scripts/_FINAL/Skills/Skills User")]
public class SkillsUser : MonoBehaviour
{

    [SerializeField]
    private List<Skill> m_Skills = new List<Skill>();

    private List<Skill> m_SkillInstances = new List<Skill>();

    #region Lifecycle

    protected virtual void Awake()
    {
        InstantiateSkills();
    }

    protected virtual void Start()
    {
        InitSkills();
    }

    protected virtual void Update()
    {
        UpdateSkills(Time.deltaTime);
    }

    #endregion


    #region Utility Methods

    public void ReplaceSkill(int _Index, Skill _NewSkill)
    {
        m_SkillInstances[_Index] = Instantiate(_NewSkill);
    }

    protected bool UseSkill(Skill _Skill, string _Data)
    {
        if (_Skill.CanUse(this, _Data))
        {
            _Skill.Use(this, _Data);
            return true;
        }
        return false;
    }

    protected void InstantiateSkills()
    {
        m_SkillInstances = new List<Skill>();
        for (int i = 0; i < m_Skills.Count; i++)
        {
            m_SkillInstances.Add(Instantiate(m_Skills[i]));
        }
    }

    protected void InitSkills()
    {
        foreach (Skill skill in m_SkillInstances)
        {
            skill.Init(this);
        }
    }

    protected void UpdateSkills(float _DeltaTime)
    {
        foreach (Skill skill in m_SkillInstances)
        {
            skill.UpdateSkill(this, null, _DeltaTime);
        }
    }

    #endregion


    #region Accessors

    protected List<Skill> SkillInstances
    {
        get { return m_SkillInstances; }
    }

    #endregion


    #region Debug & Tests

    protected virtual void OnDrawGizmos()
    {
        DrawSkillsGizmos(null);
    }

    protected void DrawSkillsGizmos(string _JsonData)
    {
        List<Skill> skills = m_SkillInstances;
        #if UNITY_EDITOR
        if (!UnityEditor.EditorApplication.isPlaying)
        {
            skills = m_Skills;
        }
        #endif

        foreach (Skill skill in skills)
        {
            skill.DrawGizmos(this, _JsonData);
        }
    }

    #endregion

}