﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class SkillMod : ScriptableObject
{

    /// <summary>
    /// Called when stats are processed by a skill user, before applying attack.
    /// </summary>
    /// <param name="_User">The skill user that uses the given skill.</param>
    /// <param name="_Skill">The skill used by the skill user.</param>
    /// <param name="_Data">The stats data before any other calculation from other mods.</param>
    /// <returns>Returns the output data.</returns>
    public virtual string OnProcessStats(SkillsUser _User, Skill _Skill, string _Data)
    {
        return null;
    }

    /// <summary>
    /// Called when the attack hits a target.
    /// </summary>
    /// <param name="_HitObject">The object that has been hit when using the given Skill.</param>
    /// <param name="_User">The user of the given skill.</param>
    /// <param name="_Skill">The skill that produces the attack.</param>
    /// <param name="_Data">Data to send to this mod (stats, effects, ...)</param>
    public virtual void OnAttackHit(GameObject _HitObject, SkillsUser _User, Skill _Skill, string _Data) { }

}