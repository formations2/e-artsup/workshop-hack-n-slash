﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Item Drag And Drop")]
public class Final_DragAndDrop : MonoBehaviour
{

    private Final_DragAndDropInfos m_DraggedItemInfos = Final_DragAndDropInfos.Empty;

    protected virtual void Update()
    {
        if(IsDraggingItem)
        {
            m_DraggedItemInfos.draggedObject.transform.position = Input.mousePosition + (m_DraggedItemInfos.originalItemPosition - m_DraggedItemInfos.mousePosition);
        }
    }

    /// <summary>
    /// Called by Draggable components when the item is dragged.
    /// </summary>
    /// <param name="_Obj">The dragged object.</param>
    /// <returns>Returns true if an object can be dragged, otherwise false</returns>
    public bool Drag(Final_Draggable _Obj)
    {
        if(!IsDraggingItem)
        {
            m_DraggedItemInfos.draggedObject = _Obj;
            m_DraggedItemInfos.mousePosition = Input.mousePosition;
            m_DraggedItemInfos.originalItemPosition = _Obj.transform.position;

            return true;
        }

        return false;
    }

    public void CancelDrag()
    {
        if(IsDraggingItem)
        {
            Debug.Log("Cancel drag");
            ResetDraggedItem();
        }
    }

    private void ResetDraggedItem()
    {
        m_DraggedItemInfos.draggedObject.CancelDrag(m_DraggedItemInfos);
        m_DraggedItemInfos = Final_DragAndDropInfos.Empty;
    }

    /// <summary>
    /// Called by a DropZone component when an object is dropped inside it.
    /// </summary>
    /// <param name="_Zone">The zone where the object is dropped.</param>
    /// <returns>Returns true if the object can be dropped, otherwise false.</returns>
    public bool Drop(Final_DropZone _Zone)
    {
        if(IsDraggingItem)
        {
            RectTransform rectTransform = _Zone.GetComponent<RectTransform>();
            if(rectTransform != null)
            {
                if(RectTransformUtility.RectangleContainsScreenPoint(rectTransform, Input.mousePosition) && _Zone.Accept(m_DraggedItemInfos.draggedObject))
                {
                    Debug.Log("Drop ok");
                    _Zone.Receive(m_DraggedItemInfos);
                    m_DraggedItemInfos = Final_DragAndDropInfos.Empty;
                    return true;
                }
            }

            Debug.Log("Drop fail");
            ResetDraggedItem();
        }

        return false;
    }

    public bool IsDraggingItem
    {
        get { return m_DraggedItemInfos.draggedObject != null; }
    }

}