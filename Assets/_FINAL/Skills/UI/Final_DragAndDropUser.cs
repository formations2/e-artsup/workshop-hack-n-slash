﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Drag And Drop User")]
public abstract class Final_DragAndDropUser : MonoBehaviour
{

    private Final_DragAndDrop m_DragAndDrop = null;

	protected virtual void Awake()
    {
        m_DragAndDrop = FindObjectOfType<Final_DragAndDrop>();
    }

    public Final_DragAndDrop DragAndDrop
    {
        get { return m_DragAndDrop; }
    }

}