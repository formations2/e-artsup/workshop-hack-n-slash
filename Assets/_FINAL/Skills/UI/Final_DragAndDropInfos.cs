﻿using UnityEngine;

public struct Final_DragAndDropInfos
{
    public static readonly Final_DragAndDropInfos Empty = new Final_DragAndDropInfos();

    public Final_Draggable draggedObject;
    public Vector3 mousePosition;
    public Vector3 originalItemPosition;
}