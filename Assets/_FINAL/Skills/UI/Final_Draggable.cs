﻿using UnityEngine;
using UnityEngine.EventSystems;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Draggable")]
public class Final_Draggable : Final_DragAndDropUser, IDragHandler, IEndDragHandler
{

    public void OnDrag(PointerEventData eventData)
    {
        DragAndDrop.Drag(this);
    }

    public virtual void CancelDrag(Final_DragAndDropInfos _Infos)
    {
        transform.position = _Infos.originalItemPosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        DragAndDrop.CancelDrag();
    }

}