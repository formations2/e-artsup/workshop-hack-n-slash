﻿using UnityEngine;
using UnityEngine.UI;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Skill Mods Sorted List (Vertical Layout)")]
public class Final_UI_SkillModsSortedList : VerticalLayoutGroup
{

    protected override void Awake()
    {
        base.Awake();

        SortChildSkillMods();
    }

    protected override void OnTransformChildrenChanged()
    {
        base.OnTransformChildrenChanged();

        SortChildSkillMods();
    }

    private void SortChildSkillMods()
    {
        Final_UI_SkillMod[] skillMods = GetComponentsInChildren<Final_UI_SkillMod>();
        System.Array.Sort(skillMods);

        for (int i = 0; i < skillMods.Length; i++)
        {
            skillMods[i].transform.SetSiblingIndex(i);
        }
    }

}