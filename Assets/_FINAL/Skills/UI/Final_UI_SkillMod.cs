﻿using System;
using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Skill Mod (draggable)")]
public class Final_UI_SkillMod : Final_Draggable, IComparable<Final_UI_SkillMod>
{

    [SerializeField]
    private int m_Priority = 0;

    public int CompareTo(Final_UI_SkillMod _Other)
    {
        return m_Priority.CompareTo(_Other.m_Priority);
    }

    public int Priority
    {
        get { return m_Priority; }
    }

}