﻿using UnityEngine;
using UnityEngine.EventSystems;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/UI/Drop Zone")]
public class Final_DropZone : Final_DragAndDropUser, IDropHandler
{

    [SerializeField]
    private Transform m_Container = null;

    protected override void Awake()
    {
        base.Awake();

        if(m_Container == null) { m_Container = GetComponent<Transform>(); }
    }

    public void OnDrop(PointerEventData _EventData)
    {
        DragAndDrop.Drop(this);
    }

    public virtual bool Accept(Final_Draggable _Obj)
    {
        return true;
    }

    public virtual void Receive(Final_DragAndDropInfos _Infos)
    {
        if(_Infos.draggedObject.transform.parent != m_Container)
        {
            _Infos.draggedObject.transform.SetParent(m_Container, false);
        }
        else
        {
            _Infos.draggedObject.transform.position = _Infos.originalItemPosition;
        }
    }

}