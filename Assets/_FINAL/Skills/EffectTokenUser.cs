﻿using System.Collections.Generic;

using UnityEngine;

///<summary>
/// 
///</summary>
public class EffectTokenUser : MonoBehaviour
{

    [SerializeField]
    private EffectToken[] m_EffectTokens = { };

    private List<EffectToken> m_EffectTokenInstances = new List<EffectToken>();

    #region Lifecycle

    protected virtual void Awake()
    {
        InitTokens(null);
    }

    protected virtual void Update()
    {
        UpdateTokens(null, Time.deltaTime);
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Adds the given token to the list if possible (uses given token's CanAddToken() method for validation).
    /// </summary>
    /// <param name="_Token">The token to add to this user.</param>
    /// <param name="_Data">Eventual data to send to the token.</param>
    /// <returns>Returns true if the token has been added successfully, otherwise false.</returns>
    public bool AddToken(EffectToken _Token, string _Data)
    {
        if (_Token.CanAddToken(this, _Data))
        {
            m_EffectTokenInstances.Add(_Token);
            return true;
        }

        return false;
    }

    #endregion

    #region Utility Methods

    /// <summary>
    /// Initialize the token instances, based on m_EffectTokens array.
    /// </summary>
    /// <param name="_Data">Eventual data to send to the tokens at initialization.</param>
    protected void InitTokens(string _Data)
    {
        m_EffectTokenInstances.Clear();
        foreach(EffectToken token in m_EffectTokens)
        {
            AddToken(token, _Data);
        }
    }

    /// <summary>
    /// Updates all tokens of the list.
    /// Note that it also removes expired tokens.
    /// </summary>
    /// <param name="_Data">Eventual data to send to the updated tokens, or their removal.</param>
    /// <param name="_DeltaTime">Elapsed time since the last frame.</param>
    protected void UpdateTokens(string _Data, float _DeltaTime)
    {
        List<int> indexesToremove = new List<int>();

        for (int i = 0; i < m_EffectTokenInstances.Count; i++)
        {
            if (m_EffectTokenInstances[i].OnUpdateToken(this, _Data, _DeltaTime))
            {
                indexesToremove.Add(i);
            }
        }

        for (int i = indexesToremove.Count - 1; i >= 0; i--)
        {
            m_EffectTokenInstances[indexesToremove[i]].OnRemoveToken(this, _Data);
            m_EffectTokenInstances.RemoveAt(indexesToremove[i]);
        }
    }

    #endregion

    #region Accessors

    /// <summary>
    /// Gets the token instances list, as an array.
    /// </summary>
    protected EffectToken[] TokenInstances
    {
        get { return m_EffectTokenInstances.ToArray(); }
    }

    #endregion

}