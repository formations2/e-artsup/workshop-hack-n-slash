﻿using UnityEngine;

///<summary>
/// 
///</summary>
[CreateAssetMenu(fileName = "NewBasicMeleeAttack", menuName = "_FINAL/Skills/Basic Melee Attack")]
public class Skill_BasicMeleeAttack : Skill
{

    [System.Serializable]
    public class BasicMeleeAttackStats
    {
        public float range;
        public float radius;
        public float force;

        public override string ToString()
        {
            return $"BasicMeleeAttackStats: range = {range}, radius = {radius}, force = {force}";
        }

        public static BasicMeleeAttackStats operator +(BasicMeleeAttackStats _Stats1, BasicMeleeAttackStats _Stats2)
        {
            return new BasicMeleeAttackStats()
            {
                range = _Stats1.range + _Stats2.range,
                radius = _Stats1.radius + _Stats2.radius,
                force = _Stats1.force + _Stats2.force
            };
        }
    }

    [SerializeField]
    private BasicMeleeAttackStats m_Stats = new BasicMeleeAttackStats();

    [SerializeField, Tooltip("Defines the layers that can receive damages from this attack")]
    private LayerMask m_TargetLayers = ~0;

    /// <summary>
    /// This attack can be ued anytime.
    /// </summary>
    public override bool CanUse(SkillsUser _User, string _Stats)
    {
        return true;
    }

    /// <summary>
    /// Inflict damages to all entities in the attack range.
    /// </summary>
    public override void Use(SkillsUser _User, string _Stats)
    {
        BasicMeleeAttackStats stats = AddUserStats(_Stats);
        ProcessStats(_User, stats);

        RaycastHit[] touchedObjects = Physics.SphereCastAll(_User.transform.position, stats.radius, _User.transform.forward, stats.range, m_TargetLayers);
        if(touchedObjects.Length > 0)
        {
            foreach(RaycastHit hitInfos in touchedObjects)
            {
                Debug.Log($"Basic melee attack inflicts {stats.force} damages to {hitInfos.collider.name} / Stats: {stats}");
            }
        }
    }

    /// <summary>
    /// Process given stats from mods.
    /// </summary>
    /// <param name="_User">The user that uses this Skill.</param>
    /// <param name="_Stats">Input stats to process.</param>
    private void ProcessStats(SkillsUser _User, BasicMeleeAttackStats _Stats)
    {
        string statsJson = JsonUtility.ToJson(_Stats);
        foreach (SkillMod mod in Mods)
        {
            string newStatsJson = mod.OnProcessStats(_User, this, statsJson);
            BasicMeleeAttackStats newStats = new BasicMeleeAttackStats();
            JsonUtility.FromJsonOverwrite(newStatsJson, newStats);

            _Stats.force += newStats.force;
            _Stats.radius += newStats.radius;
            _Stats.range += newStats.range;
        }
    }

    /// <summary>
    /// Add given stats to the current attack stats.
    /// </summary>
    private BasicMeleeAttackStats AddUserStats(string _Stats)
    {
        if(string.IsNullOrEmpty(_Stats))
        {
            return m_Stats;
        }

        BasicMeleeAttackStats stats = new BasicMeleeAttackStats();
        JsonUtility.FromJsonOverwrite(_Stats, stats);

        return m_Stats + stats;
    }

    /// <summary>
    /// Draws the attack range.
    /// </summary>
    public override void DrawGizmos(SkillsUser _User, string _Stats)
    {
        Color color = Color.red;
        color.a = .6f;
        Gizmos.color = color;

        BasicMeleeAttackStats stats = AddUserStats(_Stats);
        ProcessStats(_User, stats);

        Vector3 damagesZone = new Vector3(stats.radius, 0f, stats.range);
        Gizmos.DrawCube(_User.transform.position + _User.transform.forward * (stats.range / 2f), damagesZone);
    }

}