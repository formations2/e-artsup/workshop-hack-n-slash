﻿using UnityEngine;

///<summary>
/// 
///</summary>
[CreateAssetMenu(fileName = "NewSkillMod_StatsModifier", menuName = "_FINAL/Skill Mods/Stats Modifier")]
public class SkillMod_StatsModifier : SkillMod
{

    [System.Serializable]
    private class Stats
    {
        public float force;
        public float range;
        public float radius;
    }

    [SerializeField]
    private Stats m_StatsModifiers = new Stats();

    [SerializeField]
    private Final_EFactorType m_ModifierType = Final_EFactorType.Percents;

    public override string OnProcessStats(SkillsUser _User, Skill _Skill, string _Data)
    {
        Stats stats = new Stats();
        JsonUtility.FromJsonOverwrite(_Data, stats);

        Stats outputStats = new Stats
        {
            force = Final_Factor.GetDifference(stats.force, m_StatsModifiers.force, m_ModifierType),
            range = Final_Factor.GetDifference(stats.range, m_StatsModifiers.range, m_ModifierType),
            radius = Final_Factor.GetDifference(stats.radius, m_StatsModifiers.radius, m_ModifierType),
        };
        return JsonUtility.ToJson(outputStats);
    }

}