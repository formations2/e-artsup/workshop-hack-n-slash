﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Tests/Player Skills")]
public class Test_PlayerSkills : SkillsUser
{

    [SerializeField]
    private Final_PlayerStats m_PlayerStats = new Final_PlayerStats();

    protected override void Update()
    {
        KeyCode[] inputs = { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3 };
        for(int i = 0; i < inputs.Length; i++)
        {
            if(Input.GetKeyDown(inputs[i]))
            {
                string data = JsonUtility.ToJson(m_PlayerStats);
                if(SkillInstances.Count >= i + 1)
                {
                    UseSkill(SkillInstances[i], data);
                }
            }
        }

        base.Update();
    }

    protected override void OnDrawGizmos()
    {
        DrawSkillsGizmos(JsonUtility.ToJson(m_PlayerStats));
    }

}