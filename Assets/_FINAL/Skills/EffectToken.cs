﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class EffectToken : ScriptableObject
{

    /// <summary>
    /// Defines if this token can be added to the given user.
    /// </summary>
    /// <param name="_User">The user that has this token.</param>
    /// <param name="_Data">Eventual data to pass to this token.</param>
    /// <returns>Returns true if the token can be added, otherwise false.</returns>
    public abstract bool CanAddToken(EffectTokenUser _User, string _Data);

    /// <summary>
    /// Called when this token is added to the given user.
    /// Assumes that CanAddToken() has been called before to ensure this token can be add.
    /// </summary>
    /// <param name="_User">The user that has this token.</param>
    /// <param name="_Data">Eventual data to pass to this token.</param>
    public virtual void OnAddToken(EffectTokenUser _User, string _Data) { }

    /// <summary>
    /// Updates this token.
    /// </summary>
    /// <param name="_User">The user that has this token.</param>
    /// <param name="_Data">Eventual data to pass to this token.</param>
    /// <param name="_DeltaTime">Elapsed time since the last frame.</param>
    /// <returns>Returns true if this token can be removed, false if not:
    ///     - permanent token should never return true
    ///     - temporary tokens should return true when its timer ends
    /// </returns>
    public virtual bool OnUpdateToken(EffectTokenUser _User, string _Data, float _DeltaTime)
    {
        return true;
    }

    /// <summary>
    /// Called when this token is removed from the given user.
    /// </summary>
    /// <param name="_User">The user that has this token.</param>
    /// <param name="_Data">Eventual data to pass to this token.</param>
    public virtual void OnRemoveToken(EffectTokenUser _User, string _Data) { }

}