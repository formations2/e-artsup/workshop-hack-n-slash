﻿[System.Serializable]
public struct Final_Factor
{
    public float factor;
    public Final_EFactorType factorType;

    /// <summary>
    /// Applies a factor to the given value.
    /// </summary>
    /// <param name="_Value">Value to modify.</param>
    /// <param name="_Factor">Amount of the factor to apply on the value.</param>
    /// <param name="_FactorType">Type of the factor.</param>
    /// <param name="_Inverse">If true, the operation to use will be "inverted" (substract instead of add for a constant or percents
    /// factor, divide instead of multiply for multiplier).</param>
    /// <returns>Returns the modified value.</returns>
    public static float ApplyFactor(float _Value, float _Factor, Final_EFactorType _FactorType, bool _Inverse = false)
    {
        if(!_Inverse)
        {
            switch (_FactorType)
            {
                case Final_EFactorType.Constant:
                _Value += _Factor;
                break;

                case Final_EFactorType.Multiplier:
                _Value *= _Factor;
                break;

                case Final_EFactorType.Percents:
                _Value += _Value * _Factor / 100f;
                break;
            }
        }
        else
        {
            switch (_FactorType)
            {
                case Final_EFactorType.Constant:
                _Value -= _Factor;
                break;

                case Final_EFactorType.Multiplier:
                _Value /= _Factor;
                break;

                case Final_EFactorType.Percents:
                _Value -= _Value * _Factor / 100f;
                break;
            }
        }

        return _Value;
    }

    /// <summary>
    /// Gets the added/substracted value after applying the factor on the given value.
    /// </summary>
    /// <param name="_Value">Value to modify.</param>
    /// <param name="_Factor">Amount of the factor to apply on the value.</param>
    /// <param name="_FactorType">Type of the factor.</param>
    /// <param name="_Inverse">If true, the operation to use will be "inverted" (substract instead of add for a constant or percents
    /// factor, divide instead of multiply for multiplier).</param>
    /// <returns>Returns the difference between the given value and the value after applying the factor.</returns>
    public static float GetDifference(float _Value, float _Factor, Final_EFactorType _FactorType, bool _Inverse = false)
    {
        float val = ApplyFactor(_Value, _Factor, _FactorType, _Inverse);
        return val - _Value;
    }

    /// <summary>
    /// Apply the given factor to the given value.
    /// </summary>
    /// <param name="_Value">The value to modify.</param>
    /// <param name="_Factor">The factor to apply.</param>
    /// <returns>Returns the modified value.</returns>
    public static float operator *(float _Value, Final_Factor _Factor)
    {
        return ApplyFactor(_Value, _Factor.factor, _Factor.factorType);
    }

    /// <summary>
    /// Apply the given factor to the given value, using the "inverse" operation (substract instead of add for a constant or percents
    /// factor, divide instead of multiply for multiplier).
    /// </summary>
    /// <param name="_Value">The value to modify.</param>
    /// <param name="_Factor">The factor to apply.</param>
    /// <returns>Returns the modified value.</returns>
    public static float operator /(float _Value, Final_Factor _Factor)
    {
        return ApplyFactor(_Value, _Factor.factor, _Factor.factorType, true);
    }
}