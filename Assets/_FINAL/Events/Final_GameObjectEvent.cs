﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Final_GameObjectEvent : UnityEvent<GameObject> { }