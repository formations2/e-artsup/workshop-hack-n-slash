﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Follow Camera")]
public class Final_FollowCamera : MonoBehaviour
{

    [SerializeField]
    private Transform m_ObjectToFollow = null;

    [SerializeField]
    private bool m_EnableLerp = true;

    [SerializeField, NaughtyAttributes.EnableIf("m_EnableLerp"), Range(0f, 1f)]
    private float m_Lerp = 0.8f;

    [SerializeField, Tooltip("If false, the offset to the object to follow will be computed at Awake()")]
    private bool m_SetOffsetManually = false;

    [SerializeField, NaughtyAttributes.EnableIf("m_SetOffsetManually")]
    private Vector3 m_Offset = Vector3.zero;

    private Vector3 m_TargetPosition = Vector3.zero;

    private void Awake()
    {
        if(!m_SetOffsetManually)
        {
            m_Offset = transform.position - m_ObjectToFollow.position;
        }
    }

    private void LateUpdate()
    {
        m_TargetPosition = m_ObjectToFollow.position + m_Offset;
        if(m_EnableLerp)
        {
            transform.position = Vector3.Lerp(transform.position, m_TargetPosition, m_Lerp);
        }
        else
        {
            transform.position = m_TargetPosition;
        }
    }

}