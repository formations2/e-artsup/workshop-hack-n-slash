﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Tests/Characters/Behaviors/Attack Tester")]
public class Final_Behaviors_AttackTester : MonoBehaviour
{

    [SerializeField]
    private Final_Behaviors_Attack m_Attack = null;

    private void Awake()
    {
        if(m_Attack == null) { m_Attack = GetComponent<Final_Behaviors_Attack>(); }
    }

    private void Start()
    {
        if(m_Attack != null)
        {
            m_Attack.BeginAttack();
        }
    }

    private void Update()
    {
        if(m_Attack != null)
        {
            m_Attack.UpdateAttack(Time.deltaTime);
        }
    }

}