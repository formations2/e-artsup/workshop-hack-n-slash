﻿using System.Collections;
using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Characters/Behaviors/Attack Player")]
public class Final_Behaviors_AttackPlayer : Final_Behaviors_Attack
{

    [SerializeField]
    private Final_Player m_Player = null;

    [SerializeField]
    private float m_AttackAnticipationTime = 0.6f;

    [SerializeField]
    private float m_AttackCooldown = 1.4f;

    private Coroutine m_AttackCoroutine = null;
    private bool m_IsAttacking = false;
    private bool m_IsCooldownRunning = false;

    private void Awake()
    {
        if(m_Player == null) { m_Player = FindObjectOfType<Final_Player>(); }
    }

    public override void BeginAttack()
    {
        m_AttackCoroutine = StartCoroutine(AttackCoroutine(m_AttackAnticipationTime, m_AttackCooldown));
    }

    public override void UpdateAttack(float _DeltaTime) { }

    public override void EndAttack()
    {
        if(m_AttackCoroutine != null)
        {
            StopCoroutine(m_AttackCoroutine);
            m_AttackCoroutine = null;
        }

        m_IsAttacking = false;
        m_IsCooldownRunning = false;
    }

    private IEnumerator AttackCoroutine(float _AnticipationTime, float _CooldownDuration)
    {
        m_IsAttacking = true;
        m_IsCooldownRunning = false;

        yield return new WaitForSeconds(_AnticipationTime);

        Debug.Log("Apply damages");
        m_IsCooldownRunning = true;

        yield return new WaitForSeconds(_CooldownDuration);

        m_IsCooldownRunning = false;
        m_IsAttacking = false;

        m_AttackCoroutine = null;
    }

    public override bool IsAttacking
    {
        get { return m_IsAttacking; }
    }

    public bool IsCooldownRunning
    {
        get { return m_IsCooldownRunning; }
    }

}