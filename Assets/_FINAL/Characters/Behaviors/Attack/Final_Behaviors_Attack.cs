﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class Final_Behaviors_Attack : MonoBehaviour
{

    public virtual void BeginAttack() { }

    public abstract void UpdateAttack(float _DeltaTime);

    public virtual void EndAttack() { }

    public virtual bool IsAttacking { get; }

}