﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Tests/Characters/Behaviors/Chase Tester")]
public class Final_Behaviors_ChaseTester : MonoBehaviour
{

    [SerializeField]
    private Final_Behaviors_Chase m_Chase = null;

    private bool m_IsChasing = true;

    private void Awake()
    {
        if(m_Chase == null) { m_Chase = GetComponent<Final_Behaviors_Chase>(); }
    }

    private void Update()
    {
        if(m_Chase != null)
        {
            if(m_Chase.IsCloseEnough() && m_IsChasing)
            {
                m_Chase.EndChase();
                m_IsChasing = false;
            }
            else
            {
                if(!m_IsChasing)
                {
                    m_Chase.BeginChase();
                    m_IsChasing = true;
                }

                m_Chase.UpdateChase(Time.deltaTime);
            }
        }
    }

}