﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Characters/Behaviors/Chase Player")]
public class Final_Behaviors_ChasePlayer : Final_Behaviors_Chase
{

    [SerializeField]
    private Final_Player m_Player = null;

    [SerializeField]
    private float m_Speed = 10f;

    [SerializeField]
    private float m_MinimumDistanceToPlayer = 1f;

    private void Awake()
    {
        if(m_Player == null) { m_Player = FindObjectOfType<Final_Player>(); }
    }

    public override bool IsCloseEnough()
    {
        Vector3 toPlayer = m_Player.transform.position - transform.position;
        return toPlayer.sqrMagnitude <= Mathf.Pow(m_MinimumDistanceToPlayer, 2);
    }

    public override void UpdateChase(float _DeltaTime)
    {
        Vector3 toPlayer = m_Player.transform.position - transform.position;
        toPlayer.Normalize();
        transform.position += toPlayer * m_Speed * _DeltaTime;
        transform.forward = toPlayer;
    }

}