﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class Final_Behaviors_Chase : MonoBehaviour
{

    public virtual void BeginChase() { }

    public abstract bool IsCloseEnough();

    public abstract void UpdateChase(float _DeltaTime);

    public virtual void EndChase() { }

}