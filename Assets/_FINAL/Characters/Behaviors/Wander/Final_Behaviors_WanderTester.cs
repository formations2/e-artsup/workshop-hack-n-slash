﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Tests/Characters/Behaviors/Wander Tester")]
public class Final_Behaviors_WanderTester : MonoBehaviour
{

    [SerializeField]
    private Final_Behaviors_Wander m_Wander = null;

    private void Awake()
    {
        if(m_Wander == null) { m_Wander = GetComponent<Final_Behaviors_Wander>(); }
    }

    private void Start()
    {
        if(m_Wander != null)
        {
            m_Wander.BeginWander();
        }
    }

    private void Update()
    {
        if(m_Wander != null)
        {
            m_Wander.UpdateWander(Time.deltaTime);
        }
    }

}