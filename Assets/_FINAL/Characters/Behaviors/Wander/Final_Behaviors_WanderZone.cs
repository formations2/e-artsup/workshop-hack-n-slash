﻿using System.Collections;
using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Characters/Behaviors/Wander Zone")]
public class Final_Behaviors_WanderZone : Final_Behaviors_Wander
{

    [SerializeField]
    private Vector3 m_WanderingZone = Vector3.zero;

    [SerializeField]
    private float m_Speed = 3f;

    [SerializeField]
    private float m_ChangeTargetInterval = 8f;

    [SerializeField]
    private float m_ArrivalDistance = 0.1f;

    private Vector3 m_Origin = Vector3.zero;
    private Vector3 m_Target = Vector3.zero;

    private float m_ChangeTargetTimer = 0f;

    private void Awake()
    {
        m_Origin = transform.position;
    }

    public override void BeginWander()
    {
        m_ChangeTargetTimer = 0f;
        m_Target = transform.position;
    }

    public override void UpdateWander(float _DeltaTime)
    {
        // Process new target if needed
        m_ChangeTargetTimer += Time.deltaTime;
        if(m_ChangeTargetTimer >= m_ChangeTargetInterval)
        {
            m_Target = ComputeRandomTarget();
            m_ChangeTargetTimer = 0f;
        }

        // Apply movement to target
        Vector3 toTarget = m_Target - transform.position;
        if(toTarget.sqrMagnitude > Mathf.Pow(m_ArrivalDistance, 2))
        {
            transform.position += toTarget.normalized * m_Speed * _DeltaTime;
            transform.forward = toTarget;
        }
    }

    private Vector3 ComputeRandomTarget()
    {
        Vector3 wanderZoneExtents = m_WanderingZone / 2f;
        Vector3 target = new Vector3
        (
            Random.Range(m_Origin.x - wanderZoneExtents.x, m_Origin.x + wanderZoneExtents.x),
            Random.Range(m_Origin.y - wanderZoneExtents.y, m_Origin.y + wanderZoneExtents.y),
            Random.Range(m_Origin.z - wanderZoneExtents.z, m_Origin.z + wanderZoneExtents.z)
        );
        return target;
    }

    private void OnDrawGizmosSelected()
    {
        // Draw wander target
        Color color = Color.magenta;
        Gizmos.color = color;
        Gizmos.DrawSphere(m_Target, .25f);

        // Draw wander zone
        color.a = .3f;
        Gizmos.color = color;

        Vector3 origin = m_Origin;
        #if UNITY_EDITOR
        if(!UnityEditor.EditorApplication.isPlaying)
        {
            origin = transform.position;
        }
        #endif

        Gizmos.DrawCube(origin, m_WanderingZone);
    }

}