﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class Final_Behaviors_Wander : MonoBehaviour
{

    public virtual void BeginWander() { }

    public abstract void UpdateWander(float _DeltaTime);

    public virtual void EndWander() { }

}