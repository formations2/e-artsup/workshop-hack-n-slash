﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Tests/Behaviors/Detection Tester")]
public class Final_Behaviors_DetectionTester : MonoBehaviour
{

    [SerializeField]
    private Final_Behaviors_Detection m_Detection = null;

    private void Awake()
    {
        if(m_Detection == null) { m_Detection = GetComponent<Final_Behaviors_Detection>(); }
    }

    private void Update()
    {
        if(m_Detection.Detect(out Final_Behaviors_DetectionInfos infos))
        {
            foreach(Collider obj in infos.detectedObjects)
            {
                Debug.Log("Object in sight: " + obj);
            }
        }
    }

}