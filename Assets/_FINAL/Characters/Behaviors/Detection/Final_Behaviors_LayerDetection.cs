﻿using System.Collections.Generic;

using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Characters/Behaviors/Layer Detection")]
public class Final_Behaviors_LayerDetection : Final_Behaviors_Detection
{

    [SerializeField]
    private LayerMask m_DetectionLayer = 0;

    [SerializeField]
    private LayerMask m_RaycastLayer = ~0;

    [SerializeField]
    private float m_DetectionRange = 10f;

    [SerializeField, Range(0f, 360f)]
    private float m_DetectionAngle = 160f;

    public override bool Detect(out Final_Behaviors_DetectionInfos _Infos)
    {
        Collider[] detectedObjects = Physics.OverlapSphere(transform.position, m_DetectionRange, m_DetectionLayer);
        List<Collider> objInSight = new List<Collider>();
        _Infos = new Final_Behaviors_DetectionInfos();

        if (detectedObjects.Length > 0)
        {
            foreach(Collider obj in detectedObjects)
            {
                Vector3 toObj = obj.transform.position - transform.position;

                if (!Physics.Raycast(transform.position, toObj, m_DetectionRange, m_RaycastLayer))
                {
                    toObj.Normalize();
                    float angleToObj = Vector3.Angle(transform.forward, toObj);
                    if(angleToObj <= m_DetectionAngle / 2f)
                    {
                        Debug.Log("Object in sight: " + obj);
                        objInSight.Add(obj);
                    }
                }
            }

            _Infos.detectedObjects = objInSight.ToArray();
        }

        return objInSight.Count > 0;
    }

    private void OnDrawGizmos()
    {
        DrawDetectionCone(m_DetectionRange, m_DetectionAngle);
    }

}