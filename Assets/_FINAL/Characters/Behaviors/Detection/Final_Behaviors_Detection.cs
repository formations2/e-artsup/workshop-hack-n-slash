﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class Final_Behaviors_Detection : MonoBehaviour
{

    private const float GIZMO_ANGLE_INTERVAL = 4f;

    public abstract bool Detect(out Final_Behaviors_DetectionInfos _Infos);

    protected void DrawDetectionCone(float _DetectionRange, float _DetectionAngle)
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _DetectionRange);

        Gizmos.color = Color.red;
        Vector3 start = transform.position;
        Vector3 ray = transform.forward * _DetectionRange;

        // Draw horizontal gizmos

        // Draw first line
        float angle = -_DetectionAngle / 2f;
        float maxAngle = _DetectionAngle / 2f;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        Vector3 direction = rotation * ray;
        Gizmos.DrawLine(start, start + direction);

        while(angle < maxAngle)
        {
            rotation = Quaternion.AngleAxis(angle, Vector3.up);
            direction = rotation * ray;
            Gizmos.DrawLine(start, start + direction);

            angle += GIZMO_ANGLE_INTERVAL;
        }

        rotation = Quaternion.AngleAxis(maxAngle, Vector3.up);
        direction = rotation * ray;
        Gizmos.DrawLine(start, start + direction);

        // Draw vertical gizmos
        rotation = Quaternion.AngleAxis(-_DetectionAngle / 2f, transform.right);
        direction = rotation * ray;
        Gizmos.DrawLine(start, start + direction);

        rotation = Quaternion.AngleAxis(_DetectionAngle / 2f, transform.right);
        direction = rotation * ray;
        Gizmos.DrawLine(start, start + direction);
    }

}