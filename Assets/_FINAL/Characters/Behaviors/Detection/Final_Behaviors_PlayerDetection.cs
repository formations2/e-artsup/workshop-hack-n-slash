﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Characters/Behaviors/Player Detection")]
public class Final_Behaviors_PlayerDetection : Final_Behaviors_Detection
{

    [SerializeField]
    private Final_Player m_Player = null;

    [SerializeField]
    private float m_DetectionRange = 10f;

    [SerializeField]
    private float m_DetectionAngle = 120f;

    [SerializeField]
    private LayerMask m_RaycastLayer = ~0;

    private void Awake()
    {
        if(m_Player == null) { m_Player = FindObjectOfType<Final_Player>(); }
    }

    public override bool Detect(out Final_Behaviors_DetectionInfos _Infos)
    {
        _Infos = new Final_Behaviors_DetectionInfos();

        Vector3 toPlayer = m_Player.transform.position - transform.position;
        if(toPlayer.sqrMagnitude <= Mathf.Pow(m_DetectionRange, 2))
        {
            if(Physics.Raycast(transform.position, toPlayer, out RaycastHit hitInfos, m_DetectionRange, m_RaycastLayer))
            {
                Collider playerCollider = m_Player.GetComponent<Collider>();
                if (hitInfos.collider == playerCollider)
                {
                    toPlayer.Normalize();
                    float angleToPlayer = Vector3.Angle(transform.forward, toPlayer);
                    if (angleToPlayer <= m_DetectionAngle / 2f)
                    {
                        _Infos.detectedObjects = new Collider[] { playerCollider };
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void OnDrawGizmos()
    {
        DrawDetectionCone(m_DetectionRange, m_DetectionAngle);
        if(m_Player != null)
        {
            Vector3 toPlayer = m_Player.transform.position - transform.position;
            if (Detect(out Final_Behaviors_DetectionInfos infos))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, transform.position + toPlayer);
            }
            else if(toPlayer.sqrMagnitude <= Mathf.Pow(m_DetectionRange, 2))
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, transform.position + toPlayer);
            }
        }
    }

}