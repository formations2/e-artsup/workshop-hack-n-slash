﻿[System.Serializable]
public class Final_PlayerStats
{
    public static readonly Final_PlayerStats init = new Final_PlayerStats
    {
        maxXP = 100,
        xp = 0,
        level = 1,

        hpMax = 100,
        hp = 100,

        force = 10,
        speed = 10
    };

    // Level
    public float maxXP;
    public float xp;
    public int level;

    // Health
    public float hpMax;
    public float hp;

    // Gameplay
    public float force;
    public float speed;
}