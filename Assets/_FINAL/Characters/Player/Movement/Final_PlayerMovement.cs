﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Player/Movement")]
public class Final_PlayerMovement : MonoBehaviour
{

    [SerializeField]
    private float m_Speed = 10f;

    [SerializeField]
    private bool m_UseCameraForward = false;

    [SerializeField, NaughtyAttributes.ShowIf("m_UseCameraForward")]
    private Camera m_Camera = null;

    [SerializeField, NaughtyAttributes.ReorderableList]
    private Final_PlayerMovementStrategy[] m_MovementStrategies = { };

    private Final_PlayerMovementStrategy m_ActiveStrategy = null;

    private void Update()
    {
        foreach(Final_PlayerMovementStrategy strategy in m_MovementStrategies)
        {
            if(strategy.ShouldActivateStrategy(this))
            {
                if(m_ActiveStrategy != strategy)
                {
                    if (m_ActiveStrategy != null)
                    {
                        m_ActiveStrategy.OnDeactivateStrategy(this);
                    }
                    m_ActiveStrategy = strategy;
                    m_ActiveStrategy.OnActivateStrategy(this);
                }

                strategy.UpdateMovement(this, Time.deltaTime);
                break;
            }
        }
    }

    public float Speed
    {
        get { return m_Speed; }
    }

    public bool UseCameraForward
    {
        get { return m_UseCameraForward; }
    }

    public Vector3 CameraForward
    {
        get { return (m_Camera != null) ? m_Camera.transform.forward : Camera.main.transform.forward; }
    }

}