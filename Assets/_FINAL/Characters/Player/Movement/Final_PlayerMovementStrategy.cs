﻿using UnityEngine;

///<summary>
/// 
///</summary>
public abstract class Final_PlayerMovementStrategy : ScriptableObject
{

    public abstract void UpdateMovement(Final_PlayerMovement _Player, float _DeltaTime);

    public abstract bool ShouldActivateStrategy(Final_PlayerMovement _Player);

    public abstract void OnActivateStrategy(Final_PlayerMovement _Player);

    public abstract void OnDeactivateStrategy(Final_PlayerMovement _Player);

}