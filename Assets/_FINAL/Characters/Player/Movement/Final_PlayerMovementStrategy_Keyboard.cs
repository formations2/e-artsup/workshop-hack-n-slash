﻿using UnityEngine;
using UnityEngine.AI;

///<summary>
/// 
///</summary>
[CreateAssetMenu(fileName = "NewPlayerMovementStrategy_Keyboard", menuName = "_FINAL/Player Movement Strategy/Keyboard")]
public class Final_PlayerMovementStrategy_Keyboard : Final_PlayerMovementStrategy
{

    public override bool ShouldActivateStrategy(Final_PlayerMovement _Player)
    {
        Vector3 direction = new Vector3
        (
            Input.GetAxis("Horizontal"),
            0f,
            Input.GetAxis("Vertical")
        );
        return direction != Vector3.zero;
    }

    public override void UpdateMovement(Final_PlayerMovement _Player, float _DeltaTime)
    {
        Vector3 direction = new Vector3
        (
            Input.GetAxis("Horizontal"),
            0f,
            Input.GetAxis("Vertical")
        );

        if(_Player.UseCameraForward)
        {
            direction.x *= _Player.CameraForward.x;
            direction.z *= _Player.CameraForward.z;
        }

        Move(_Player, direction, _DeltaTime);
    }

    public void Move(Final_PlayerMovement _Player, Vector3 _Direction, float _DeltaTime)
    {
        Rigidbody rigidbody = _Player.GetComponent<Rigidbody>();
        if (rigidbody)
        {
            Vector3 expectedVelocity = Vector3.zero;

            if (_Direction != Vector3.zero)
            {
                _Direction = Vector3.ClampMagnitude(_Direction, 1f);
                expectedVelocity = _Direction * _Player.Speed;
                _Player.transform.forward = _Direction;
            }

            expectedVelocity.y = rigidbody.velocity.y;
            rigidbody.velocity = expectedVelocity;
        }
    }

    public override void OnActivateStrategy(Final_PlayerMovement _Player)
    {
        Rigidbody rigidbody = _Player.GetComponent<Rigidbody>();
        if (rigidbody)
        {
            rigidbody.isKinematic = false;
        }
    }

    public override void OnDeactivateStrategy(Final_PlayerMovement _Player)
    {
        Rigidbody rigidbody = _Player.GetComponent<Rigidbody>();
        if (rigidbody)
        {
            rigidbody.isKinematic = true;
        }
    }

}