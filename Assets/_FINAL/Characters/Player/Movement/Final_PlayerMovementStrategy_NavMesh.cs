﻿using UnityEngine;
using UnityEngine.AI;

///<summary>
/// 
///</summary>
[CreateAssetMenu(fileName = "NewPlayerMovementStrategy_NavMesh", menuName = "_FINAL/Player Movement Strategy/Nav Mesh")]
public class Final_PlayerMovementStrategy_NavMesh : Final_PlayerMovementStrategy
{

    public override bool ShouldActivateStrategy(Final_PlayerMovement _Player)
    {
        return Input.GetMouseButtonDown(1);
    }

    public override void UpdateMovement(Final_PlayerMovement _Player, float _DeltaTime)
    {
        if(Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit))
            {
                MoveTo(_Player, hit.point);
            }
        }
    }

    public void MoveTo(Final_PlayerMovement _Player, Vector3 _Target)
    {
        NavMeshAgent agent = _Player.GetComponent<NavMeshAgent>();
        if(agent)
        {
            agent.isStopped = false;
            agent.SetDestination(_Target);
        }
    }

    public override void OnActivateStrategy(Final_PlayerMovement _Player)
    {
        NavMeshAgent agent = _Player.GetComponent<NavMeshAgent>();
        if (agent)
        {
            agent.enabled = true;
            agent.isStopped = true;

            agent.speed = _Player.Speed;
            agent.acceleration = Mathf.Pow(_Player.Speed, 2);
        }
    }

    public override void OnDeactivateStrategy(Final_PlayerMovement _Player)
    {
        NavMeshAgent agent = _Player.GetComponent<NavMeshAgent>();
        if (agent)
        {
            agent.isStopped = true;
            agent.enabled = false;
        }
    }

}