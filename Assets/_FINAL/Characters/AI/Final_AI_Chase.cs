﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("_FINAL/AI/Chase")]
public class Final_AI_Chase : StateMachineBehaviour
{

	// Called on the first Update frame when a statemachine evaluate this state.
	public override void OnStateEnter(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Chase chase = _Animator.GetComponent<Final_Behaviors_Chase>();
        if(chase != null)
        {
            chase.BeginChase();
        }
    }

	// Called at each Update frame except for the first and last frame.
	public override void OnStateUpdate(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Chase chase = _Animator.GetComponent<Final_Behaviors_Chase>();
        if (chase != null)
        {
            bool isCloseEnough = chase.IsCloseEnough();
            _Animator.SetBool("isCloseEnough", isCloseEnough);

            if (!isCloseEnough)
            {
                chase.UpdateChase(Time.deltaTime);
            }
        }
    }

	// Called on the last update frame when a statemachine evaluate this state.
	public override void OnStateExit(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Chase chase = _Animator.GetComponent<Final_Behaviors_Chase>();
        if (chase != null)
        {
            chase.EndChase();
            _Animator.SetBool("isCloseEnough", false);
        }
    }

	// Called right after MonoBehaviour.OnAnimatorMove.
	//public override void OnStateMove(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

	// Called right after MonoBehaviour.OnAnimatorIK.
	//public override void OnStateIK(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

}