﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("_FINAL/AI/Attack")]
public class Final_AI_Attack : StateMachineBehaviour
{

	// Called on the first Update frame when a statemachine evaluate this state.
	public override void OnStateEnter(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Attack attack = _Animator.GetComponent<Final_Behaviors_Attack>();
        if(attack != null)
        {
            attack.BeginAttack();
            _Animator.SetBool("isAttacking", attack.IsAttacking);
        }
    }

	// Called at each Update frame except for the first and last frame.
	public override void OnStateUpdate(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Attack attack = _Animator.GetComponent<Final_Behaviors_Attack>();
        if (attack != null)
        {
            attack.UpdateAttack(Time.deltaTime);
            _Animator.SetBool("isAttacking", attack.IsAttacking);
        }
    }

	// Called on the last update frame when a statemachine evaluate this state.
	public override void OnStateExit(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Attack attack = _Animator.GetComponent<Final_Behaviors_Attack>();
        if (attack != null)
        {
            attack.EndAttack();
        }
    }

	// Called right after MonoBehaviour.OnAnimatorMove.
	//public override void OnStateMove(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

	// Called right after MonoBehaviour.OnAnimatorIK.
	//public override void OnStateIK(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

}