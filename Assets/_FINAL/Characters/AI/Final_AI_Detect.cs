﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("_FINAL/AI/Detect")]
public class Final_AI_Detect : StateMachineBehaviour
{

	// Called on the first Update frame when a statemachine evaluate this state.
	public override void OnStateEnter(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Detection detect = _Animator.GetComponent<Final_Behaviors_Detection>();
        if (detect != null)
        {
            _Animator.SetBool("detectSomething", detect.Detect(out Final_Behaviors_DetectionInfos infos));
        }
    }

	// Called at each Update frame except for the first and last frame.
	public override void OnStateUpdate(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        Final_Behaviors_Detection detect = _Animator.GetComponent<Final_Behaviors_Detection>();
        if (detect != null)
        {
            _Animator.SetBool("detectSomething", detect.Detect(out Final_Behaviors_DetectionInfos infos));
        }
    }

	// Called on the last update frame when a statemachine evaluate this state.
	public override void OnStateExit(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex)
    {
        _Animator.SetBool("detectSomething", false);
    }

	// Called right after MonoBehaviour.OnAnimatorMove.
	//public override void OnStateMove(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

	// Called right after MonoBehaviour.OnAnimatorIK.
	//public override void OnStateIK(Animator _Animator, AnimatorStateInfo _StateInfo, int _LayerIndex){}

}