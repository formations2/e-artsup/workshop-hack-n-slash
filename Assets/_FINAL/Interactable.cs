﻿using UnityEngine;
using UnityEngine.Events;

///<summary>
/// 
///</summary>
//[AddComponentMenu("Scripts/Interactable")]
public class Interactable : MonoBehaviour
{

    [SerializeField]
    private Final_GameObjectEvent m_OnBeginInteraction = new Final_GameObjectEvent();

    [SerializeField]
    private Final_GameObjectEvent m_OnEndInteraction = new Final_GameObjectEvent();

    private GameObject m_Other = null;

    public void Interact(GameObject _Other)
    {
        m_Other = _Other;
        m_OnBeginInteraction.Invoke(_Other);
    }

    public void StopInteract()
    {
        m_Other = null;
        m_OnEndInteraction.Invoke(m_Other);
    }

}