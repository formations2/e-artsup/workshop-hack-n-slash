﻿using UnityEngine;

///<summary>
/// 
///</summary>
[AddComponentMenu("Scripts/_FINAL/Fancy/Random Spawner")]
public class Final_RandomSpawner : MonoBehaviour
{

    [SerializeField]
    private GameObject[] m_ObjectsToSpawn = { };

    [SerializeField]
    private bool m_RandomizeRotation = false;

    [SerializeField]
    private bool m_RandomizePosition = true;

    [SerializeField]
    private bool m_RandomizeScale = false;

    [SerializeField, NaughtyAttributes.EnableIf("m_RandomizeScale")]
    private bool m_UniformRandomScale = true;

    [SerializeField, NaughtyAttributes.EnableIf("m_RandomizeScale"), NaughtyAttributes.ShowIf("m_UniformRandomScale")]
    private float m_MinScaleFactor = 1f;

    [SerializeField, NaughtyAttributes.EnableIf("m_RandomizeScale"), NaughtyAttributes.ShowIf("m_UniformRandomScale")]
    private float m_MaxScaleFactor = 1f;

    [SerializeField, NaughtyAttributes.EnableIf("m_RandomizeScale"), NaughtyAttributes.HideIf("m_UniformRandomScale")]
    private Vector3 m_ScaleOffset = Vector3.zero;

    [SerializeField]
    private int m_NumberOfSpawns = 10;

    [SerializeField]
    private Vector3 m_SpawnZone = Vector3.one;

    [SerializeField]
    private bool m_ReparentSpawnedObjects = false;

    [SerializeField]
    private bool m_SpawnOnAwake = true;

    private void Awake()
    {
        if(m_SpawnOnAwake)
        {
            Spawn();
        }
    }

    [NaughtyAttributes.Button("Spawn objects")]
    public void Spawn()
    {
        Vector3 spawnZoneExtents = m_SpawnZone / 2;
        for (int i = 0; i < m_NumberOfSpawns; i++)
        {
            GameObject obj = m_ObjectsToSpawn[Random.Range(0, m_ObjectsToSpawn.Length)];

            Vector3 position = transform.position;
            if (m_RandomizePosition)
            {
                position = new Vector3
                (
                    Random.Range(-spawnZoneExtents.x, spawnZoneExtents.x),
                    Random.Range(-spawnZoneExtents.y, spawnZoneExtents.y),
                    Random.Range(-spawnZoneExtents.z, spawnZoneExtents.z)
                );
            }

            Quaternion rotation = Quaternion.identity;
            if (m_RandomizeRotation)
            {
                rotation = Random.rotation;
            }

            GameObject instance = Instantiate(obj, position, rotation, (m_ReparentSpawnedObjects) ? transform : null);

            if (m_RandomizeScale)
            {
                Vector3 scale = instance.transform.localScale;

                if (m_UniformRandomScale)
                {
                    scale *= Random.Range(m_MinScaleFactor, m_MaxScaleFactor);
                }
                else
                {
                    Vector3 scaleOffsetExtents = m_ScaleOffset / 2f;
                    scale.x += Random.Range(-scaleOffsetExtents.x, scaleOffsetExtents.x);
                    scale.y += Random.Range(-scaleOffsetExtents.y, scaleOffsetExtents.y);
                    scale.z += Random.Range(-scaleOffsetExtents.z, scaleOffsetExtents.z);
                }

                instance.transform.localScale = scale;
            }
        }
    }

    [NaughtyAttributes.Button("Destroy child objects")]
    private void RemoveChildren()
    {
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        for(int i = 0; i < children.Length; i++)
        {
            if(children[i] != transform)
            {
                #if UNITY_EDITOR
                if(Application.isPlaying)
                {
                    Destroy(children[i].gameObject);
                }
                else
                {
                    DestroyImmediate(children[i].gameObject);
                }
                #else
                Destroy(children[i].gameObject);
                #endif
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Color color = Color.yellow;
        color.a = .5f;
        Gizmos.color = color;

        Gizmos.DrawCube(transform.position, m_SpawnZone);
    }

}